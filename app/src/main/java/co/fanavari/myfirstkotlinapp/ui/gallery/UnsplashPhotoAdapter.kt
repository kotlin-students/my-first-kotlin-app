package co.fanavari.myfirstkotlinapp.ui.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.fanavari.myfirstkotlinapp.R
import co.fanavari.myfirstkotlinapp.data.UnsplashPhoto
import co.fanavari.myfirstkotlinapp.databinding.ItemUnsplashBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class UnsplashPhotoAdapter : PagingDataAdapter<UnsplashPhoto,
        UnsplashPhotoAdapter.PhotoViewHolder>(
    PHOTO_COMPARATOR
        ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
       val binding = ItemUnsplashBinding.inflate(
           LayoutInflater.from(parent.context),
           parent, false
       )
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {

        val currentItem = getItem(position)

        currentItem?.let { holder.bind(it) }

    }

    class PhotoViewHolder(private val binding: ItemUnsplashBinding) :
            RecyclerView.ViewHolder(binding.root){
                fun bind(photo: UnsplashPhoto){
                    binding.apply {
                       Glide.with(itemView)
                           .load(photo.urls.regular)
                           .centerCrop()
                           .transition(DrawableTransitionOptions.withCrossFade())
                           .error(R.drawable.ic_error)
                           .into(animalImageView)

                        titleAnimal.text = photo.user.username

                    }
                }
            }

    companion object{
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<UnsplashPhoto>(){
            override fun areItemsTheSame(oldItem: UnsplashPhoto, newItem: UnsplashPhoto): Boolean
            = oldItem.id == newItem.id
            override fun areContentsTheSame(
                oldItem: UnsplashPhoto,
                newItem: UnsplashPhoto
            ) = oldItem == newItem

        }
    }
}