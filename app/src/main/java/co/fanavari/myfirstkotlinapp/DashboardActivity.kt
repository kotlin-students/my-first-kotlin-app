package co.fanavari.myfirstkotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import co.fanavari.myfirstkotlinapp.databinding.ActivityDashboardBinding


import com.google.android.material.card.MaterialCardView

class DashboardActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDashboardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_dashboard)
        binding = ActivityDashboardBinding.inflate(layoutInflater)

       // binding = DataBindingUtil.setContentView(this,R.layout.activity_dashboard)

        setContentView(binding.root)

//         val backB: AppCompatImageButton = findViewById(R.id.backB)
        //val logoutB: AppCompatImageButton = findViewById(R.id.logOutB)

        //val todoB: MaterialButton = findViewById(R.id.todoB)

        val layoutCard: MaterialCardView = findViewById(R.id.layoutCards)

        binding.backB.setOnClickListener {
            Toast.makeText(this, "back Button clicked!", Toast.LENGTH_SHORT).show()
        }

        binding.lifeCycleCard.setOnClickListener {
            val message: String = binding.textViewClassMentorName.text.toString()

            val intent = Intent(this,IntentExampleActivity::class.java)
            intent.putExtra(Constants.MENTOR_NAME, message)
            intent.putExtra("id",12)

            startActivity(intent)
        }

        layoutCard.setOnClickListener {
           // Toast.makeText(this,R.string.test_app,Toast.LENGTH_LONG).show()
            showToast(R.string.test_app.toString())
        }

        binding.layoutCards.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }

        binding.archCard.setOnClickListener {
            val intent = Intent(this,ComponentExampleActivity::class.java)
            startActivity(intent)
        }
    }

    fun openMainActivity(view: View){
        /*val intent = Intent(this,MainActivity::class.java)

        startActivity(intent)*/
    }
}