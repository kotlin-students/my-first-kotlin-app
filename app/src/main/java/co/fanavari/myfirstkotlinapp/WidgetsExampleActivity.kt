package co.fanavari.myfirstkotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.system.Os.accept
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.RadioButton
import co.fanavari.myfirstkotlinapp.databinding.ActivityWidgetsExampleBinding
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.chip.Chip
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar

class WidgetsExampleActivity : AppCompatActivity(),  AdapterView.OnItemSelectedListener {
    private lateinit var binding: ActivityWidgetsExampleBinding
    private lateinit var planetsArray: Array<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWidgetsExampleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bar = binding.bottomAppBar

        planetsArray = resources.getStringArray(R.array.planets_array)

        bar.setOnMenuItemClickListener{
            when (it.itemId){
                R.id.search -> {
                    //Handle search icon press
                    true
                }
                R.id.more -> {
                    // Handle more itme
                    true
                }
                else -> false
            }

        }

        bar.setNavigationOnClickListener {
            if(bar.fabAlignmentMode == BottomAppBar.FAB_ALIGNMENT_MODE_CENTER)
                bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
            else
                bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
        }

        binding.floatingActionButton.setOnClickListener {
            if(bar.fabAlignmentMode == BottomAppBar.FAB_ALIGNMENT_MODE_CENTER)
                bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
            else
                bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
        }

        binding.topAppBar.setNavigationOnClickListener {
            // Handle navigation icon press
        }

        binding.topAppBar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.favorite -> {
                    // Handle favorite icon press
                    true
                }
                R.id.search -> {
                    // Handle search icon press
                    true
                }
                R.id.more -> {
                    // Handle more item (inside overflow menu) press
                    true
                }
                else -> false
            }
        }

        binding.toggleButton.addOnButtonCheckedListener { _, checkedId, isChecked ->
            if(isChecked)
                showToast(isChecked.toString())
            if(checkedId == R.id.button1Toggle) {
                showToast(checkedId.toString())
                binding.checkbox1material.isChecked = false
                binding.checkbox4material.isEnabled = true
            }
            // Respond to button selection
        }

        binding.checkbox1material.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked)
                showToast(isChecked.toString())
        }

        binding.radioGender.setOnCheckedChangeListener { _, isChecked ->
            showToast("option $isChecked is selected.")
            when (isChecked) {
                R.id.radio_female ->
                   showToast("female")
                R.id.radio_male ->
                    showToast("male")
            }
        }

        val spinner = binding.planetsSpinner
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.planets_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        spinner.onItemSelectedListener = this


        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.title_dialog))
            .setMessage(resources.getString(R.string.supporting_text))
            .setNeutralButton(resources.getString(R.string.cancel)) { dialog, which ->
                // Respond to neutral button press
            }
            .setNegativeButton(resources.getString(R.string.decline)) { dialog, which ->
                // Respond to negative button press
                binding.switch1.isChecked = false
            }
            .setPositiveButton(resources.getString(R.string.accept)) { dialog, which ->
                // Respond to positive button press
            }
            .show()


// To listen for a switch's checked/unchecked state changes
        binding.switch1.setOnCheckedChangeListener { buttonView, isChecked ->
            // Responds to switch being checked/unchecked
        }

        binding.chipGroupFilter.setOnCheckedChangeListener { group, checkedId ->
            val chip: Chip? = group.findViewById(checkedId)

        }

    }

    fun onCheckboxClicked(view: View) {
        if (view is CheckBox) {
            val checked: Boolean = view.isChecked

            when (view.id) {
                R.id.checkbox1material -> {
                    if (checked) {
                        // Put some meat on the sandwich
                        Snackbar.make(binding.linearWidgetActivity, R.string.android_class, Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action"){

                            }
                            .show()
                    } else {
                        // Remove the meat
                    }
                }
                R.id.checkbox4material -> {
                    if (checked) {
                        // Cheese me
                    } else {
                        // I'm lactose intolerant
                    }
                }
                // TODO: Veggie sandwich
            }
        }
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.radio_female ->
                    if (checked) {
                        // do every thing needed
                    }
                R.id.radio_male ->
                    if (checked) {
                        // do every thing need
                    }
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        //TODO("Not yet implemented")
        showToast("item in $pos selected")
        showToast("item in ${planetsArray[pos]} selected")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        //TODO("Not yet implemented")
    }
}