package co.fanavari.myfirstkotlinapp.api

import co.fanavari.myfirstkotlinapp.data.UnsplashPhoto

data class UnsplashResponse (
    val results: List<UnsplashPhoto>
        )