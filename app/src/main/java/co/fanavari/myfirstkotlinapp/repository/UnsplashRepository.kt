package co.fanavari.myfirstkotlinapp.repository



import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import co.fanavari.myfirstkotlinapp.api.UnsplashApi
import co.fanavari.myfirstkotlinapp.data.UnsplashPagingSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UnsplashRepository @Inject constructor(
    private val unsplashApi: UnsplashApi) {

    fun getSearchResults(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize =  100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {UnsplashPagingSource(
                unsplashApi, query)}
        ).liveData

}