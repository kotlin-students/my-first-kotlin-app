package co.fanavari.myfirstkotlinapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.InetAddresses
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import co.fanavari.myfirstkotlinapp.databinding.ActivityIntentExampleBinding
import javax.security.auth.Subject

class IntentExampleActivity : AppCompatActivity() {
    private lateinit var binding: ActivityIntentExampleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntentExampleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle: Bundle? = intent.extras

        bundle?.let {
            val message = bundle.getString(Constants.MENTOR_NAME) + bundle.getInt("id")
            binding.textViewMentorName.text = message
            showToast(message)
        }

        val openIntentForResultExampleActivity =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == Activity.RESULT_OK) {
                    showToast(it.data?.getStringExtra("id").toString())
                }
            }

        binding.buttonStartActivityForResult.setOnClickListener {
            openIntentForResultExampleActivity.launch(
                Intent(this, IntentForResultExampleActivity::class.java).apply {
                    putExtra("ID", "1")
                }
            )
        }

        binding.buttonOpenURL.setOnClickListener {
            openUrl()
        }

        binding.buttonSendEmail.setOnClickListener {
            composeEmail(arrayOf("hanane.rahmatzehi@gmail.com"),"send email to my students")
        }

        binding.buttonSetAlarm.setOnClickListener {
            createAlarm("message",5,24)
        }

        binding.buttonLifecycleActivity.setOnClickListener {
            val intent = Intent(this,LifeCycleExampleActivity::class.java)
            startActivity(intent)
        }

        binding.buttonOpenWidgets.setOnClickListener {
            val intent = Intent(this,WidgetsExampleActivity::class.java)
            startActivity(intent)
        }


    }

    fun openUrl() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://fanavari.co/android-course/"))
        startActivity(intent)
    }

    fun composeEmail(addresses: Array<String>, subject: String){
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL,addresses)
            putExtra(Intent.EXTRA_SUBJECT, subject)
        }
        if (intent.resolveActivity(packageManager) != null){
            startActivity(intent)
        }
    }

    fun createAlarm(message: String, hour: Int, minutes: Int) {
        val intent = Intent(AlarmClock.ACTION_SET_ALARM).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, message)
            putExtra(AlarmClock.EXTRA_HOUR, hour)
            putExtra(AlarmClock.EXTRA_MINUTES, minutes)
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

}