package co.fanavari.myfirstkotlinapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import co.fanavari.myfirstkotlinapp.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_first, container, false)

        val view = binding.root

        binding.textViewFragment1.setOnClickListener {
           //Navigation.findNavController(view).navigate(R.id.navigateToSecondFragment)
            val action = FirstFragmentDirections.navigateToSecondFragment()
            Navigation.findNavController(view).navigate(action)
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}